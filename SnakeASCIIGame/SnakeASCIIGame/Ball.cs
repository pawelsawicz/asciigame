﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SnakeASCIIGame
{
    public class Ball : Shape
    {
        public Ball(SpriteFont font, Rectangle screenBounds)
            : base(font, screenBounds)
        {
            Speed = 1.2f;
            Content = "*";
            bounds = new Rectangle(0, 0, (int)SizeOfString.X, (int)SizeOfString.Y);
        }
        Rectangle bounds;
        bool collided;

        public Rectangle Bound
        {
            get
            {
                bounds.X = (int)Position.X;
                bounds.Y = (int)Position.Y;
                return bounds;
            }
        }

        public override void Update()
        {
            base.Update();
            collided = false;
            var temp = Position;
            temp += (Motion * Speed);
            Position = temp;
            CheckWallCollision();
        }

        private void CheckWallCollision()
        {
            var temp = Position;
            var tempSecond = Motion;

            if (Position.X < 0)
            {
                temp.X = 0;
                tempSecond.X *= -1;
                Position = temp;
                Motion = tempSecond;
            }
            if (Position.X + SizeOfString.X > ScreenBounds.Width)
            {
                tempSecond.X *= -1;                
                Motion = tempSecond;
            }
            if (Position.Y < 0)
            {
                temp.Y = 0;
                tempSecond.Y *= -1;
                Position = temp;
                Motion = tempSecond;
            }
        }

        public void SetStartPosition(Rectangle paddleLocation)
        {
            Motion = new Vector2(1, -1);
            var temp = Position;
            temp.Y = paddleLocation.Y - 20f;
            temp.X = paddleLocation.X - 20f;
            Position = temp;
        }

        public bool OffBottom()
        {
            if (Position.Y > ScreenBounds.Height)
            {
                return true;
            }

            return false;
        }

        public void PaddleCollision(Rectangle paddleLocation)
        {
            Rectangle ballLocation = new Rectangle(
                (int)Position.X,
                (int)Position.Y,
                (int)SizeOfString.X,
                (int)SizeOfString.Y);
            if (paddleLocation.Intersects(ballLocation))
            {
                var temp = Position;
                var tempSecond = Motion;
                temp.Y = paddleLocation.Y - SizeOfString.Y;
                tempSecond.Y *= -1;
                Position = temp;
                Motion = tempSecond;
            }
        }

        public void Deflection(Brick brick)
        {
            if (!collided)
            {
                var temp = Motion;
                temp.Y *= -1;
                Motion = temp;
                collided = true;
            }
        }
    }
}
