﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SnakeASCIIGame
{
    public class Paddle : Shape
    {
        public Paddle(SpriteFont font, Rectangle screenBounds)
            : base(font, screenBounds)
        {
            Content = "==========";
            SetStartPosition();
            Speed = 3f;
        }

        public void SetStartPosition()
        {    
            var temp = Position;
            temp.X = (ScreenBounds.Width / 2);
            temp.Y = (ScreenBounds.Height / 2) + 150f;
            Position = temp;
        }

        public override void Update()
        {
            base.Update();
            Motion = Vector2.Zero;

            KeyboardState = Keyboard.GetState();

            if (KeyboardState.IsKeyDown(Keys.Left))
            {
                var temp = Motion;
                temp.X = -1;
                Motion = temp;

            }
            if (KeyboardState.IsKeyDown(Keys.Right))
            {
                var temp = Motion;
                temp.X = 1;
                Motion = temp;

            }

            var tempSecond = Motion;
            tempSecond.X *= Speed;
            Motion = tempSecond;

            Position += Motion;
            LockPaddle();
        }

        public void LockPaddle()
        {
            if (Position.X < 0)
            {
                var temp = Position;
                temp.X = 0;
                Position = temp;
            }
            if (Position.X + SizeOfString.X > ScreenBounds.Width)
            {
                var temp = Position;
                temp.X = ScreenBounds.Width - SizeOfString.X;
                Position = temp;
            }
        }

        public Rectangle GetBounds()
        {
            return new Rectangle(
            (int)Position.X,
            (int)Position.Y,
            (int)SizeOfString.X,
            (int)SizeOfString.Y);
        }

        

    }
}
