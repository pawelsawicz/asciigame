﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SnakeASCIIGame
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private SpriteFont font;
        SpriteFont fontSecond;
        Rectangle screenRectangle;
        LevelMamager manager;
        GUI Gui;
        UserInfo userInfo;
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            screenRectangle = new Rectangle(
                0,
                0,
                graphics.PreferredBackBufferWidth,
                graphics.PreferredBackBufferHeight);


        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            font = Content.Load<SpriteFont>("Fonts/Microsoft Sans Serif");
            fontSecond = Content.Load<SpriteFont>("Fonts/Arial");
            
            // TODO: use this.Content to load your game content here
            manager = new LevelMamager()
            {
                Ball = new Ball(font, screenRectangle),
                Paddle = new Paddle(font, screenRectangle),
                Generator = new ObstaclesGenerator(),
                FontSecond = fontSecond
            };

            manager.StartGame(font, screenRectangle);
            userInfo = new UserInfo();
            Gui = new GUI()
            {
                Font = font,
                UserInfo = userInfo
            };
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            manager.Paddle.Update();
            manager.Ball.Update();

            manager.Ball.PaddleCollision(manager.Paddle.GetBounds());

            foreach (var item in manager.Generator.Bricks)
            {
                if (item.CheckCollision(manager.Ball))
                {
                    userInfo.Points += 2;
                    manager.CreateUpgrade(font, screenRectangle, item);
                }
            }

            /*if (manager.Ball.Position.Y <= screenRectangle.Y)
            {
                manager.StartGame(font, screenRectangle);
            }*/


            foreach (var item in manager.Upgrades)
            {
                item.Update();
                if (item.PaddleCollision(manager.Paddle.GetBounds()))
                {
                    switch (item.TypeOfUpgrade)
                    {
                        case 1:
                            userInfo.Points += 10;
                            manager.Upgrade();
                            break;
                        case 2:
                            userInfo.Points -= 10;
                            manager.Downgrade();
                            break;
                        case 37:
                            userInfo.Points -= 25;
                            manager.Slower();
                            break;
                        case 39:
                            userInfo.Points += 25;
                            manager.Faster();
                            break;
                        case 47:
                            userInfo.Points += 25;
                            manager.FasterBall();
                            break;
                        case 59:
                            userInfo.Points -= 75;
                            manager.SlowerBall();
                            break;
                        case 69:
                            userInfo.Points += 75;
                            manager.BiggerBall();
                            break;
                        default:
                            break;
                    }
                }
            }

            if (manager.Ball.OffBottom())
            {
                manager.StartGame(font, screenRectangle);
            }
            Gui.UpdateGUI();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();

            manager.Paddle.Draw(spriteBatch);
            manager.Ball.Draw(spriteBatch);

            foreach (var item in manager.Generator.Bricks)
            {
                item.Draw(spriteBatch);
            }


            foreach (var item in manager.Upgrades)
            {
                item.Draw(spriteBatch);
            }

            Gui.DrawGUI(spriteBatch);

            spriteBatch.End();

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }


    }
}
