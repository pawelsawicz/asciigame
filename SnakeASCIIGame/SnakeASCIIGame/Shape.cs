﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Threading;

namespace SnakeASCIIGame
{
    public class Shape
    {
        public Vector2 Position { get; set; }
        public Vector2 Motion { get; set; }
        public float Speed { get; set; }
        public KeyboardState KeyboardState { get; set; }
        public SpriteFont Font { get; set; }
        public Rectangle ScreenBounds { get; set; }
        public string Content { get; set; }
        public Vector2 SizeOfString { get; set; }

        public Shape(SpriteFont font, Rectangle screenBounds)
        {
            this.Font = font;
            this.ScreenBounds = screenBounds;          
            
        }

        public virtual void Update()
        {
            SizeOfString = Font.MeasureString(Content);
        }
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(Font, Content, Position, Color.Green);
        }       

    }
}
