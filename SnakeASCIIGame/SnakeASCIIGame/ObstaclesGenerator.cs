﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Threading;

namespace SnakeASCIIGame
{
    public class ObstaclesGenerator
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public Brick[,] Bricks { get; set; }
        public SpriteFont Font { get; set; }
        public Rectangle screenRectangle { get; set; }


        public void Generate()
        {

            Bricks = new Brick[Width, Height];


            for (int i = 0; i < Height; i++)
            {
                Color color;                
                string context;
                Thread.Sleep(200);
                switch (new Random().Next(1,10))
                {                    
                    case 2:
                        color = Color.Bisque;
                        context = "#";                        
                        break;
                    case 3:
                        color = Color.Chocolate;
                        context = "&";                        
                        break;
                    case 4:
                        color = Color.Cornsilk;
                        context = "$";                        
                        break;                                   
                    default:
                        color = Color.Crimson;
                        context = "@";                        
                        break;                        
                }

                for (int j = 0; j < Width; j++)
                {
                    Bricks[j, i] = new Brick(Font, screenRectangle, new Rectangle(100 + (20 * j), 100 + (20 * i), 15, 15),color, context);
                }
            }

        }
    }
}
