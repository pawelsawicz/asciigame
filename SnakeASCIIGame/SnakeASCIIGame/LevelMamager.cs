﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SnakeASCIIGame
{
    public class LevelMamager
    {
        public bool NextLevel { get; set; }
        public bool StartOver { get; set; }

        public Ball Ball { get; set; }
        public Paddle Paddle { get; set; }
        public ObstaclesGenerator Generator { get; set; }
        public List<Upgrade> Upgrades { get; set; }
        public PaddleUpgrade PaddleUpgrades { get; set; }

        public SpriteFont FontSecond { get; set; }

        public void StartGame(SpriteFont font, Rectangle screenRectangle)
        {
            Upgrades = new List<Upgrade>();
            Generator = new ObstaclesGenerator()
            {
                Font = font,
                screenRectangle = screenRectangle,
                Height = new Random().Next(5,12),
                Width = new Random().Next(25,35)

            };

            Generator.Generate();
            Ball.SetStartPosition(Paddle.GetBounds());

        }

        public void CreateUpgrade(SpriteFont font, Rectangle screenRectangle, Brick deadBrick)
        {
            int i = new Random().Next(1, 100);
            Upgrades.Add(new Upgrade(font, screenRectangle, deadBrick.Location, i));
        }

        public void Upgrade()
        {
            Paddle.Content += "=";
        }
        public void Downgrade()
        {
            Paddle.Content = "==";
        }
        public void Faster()
        {
            Paddle.Speed += 0.1f;
        }
        public void Slower()
        {
            Paddle.Speed -= 0.3f;
        }
        public void FasterBall()
        {
            Ball.Speed += 0.3f;
        }
        public void SlowerBall()
        {
            Ball.Speed -= 0.5f;
        }
        public void BiggerBall()
        {
            Ball.Font = FontSecond;
        }
        

    }
}
