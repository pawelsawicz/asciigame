﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SnakeASCIIGame
{
    public class GUI
    {
        public SpriteFont Font { get; set; }
        public UserInfo UserInfo { get; set; }
        public string InfoPoints { get; set; }

        public void DrawGUI(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(Font, "Hackaton Wroclaw 03.2013", new Vector2(300,50), Color.White);
            spriteBatch.DrawString(Font, InfoPoints, new Vector2(600, 50), Color.White);
        }

        public void UpdateGUI()
        {
            InfoPoints = string.Format("Your Points : {0}", UserInfo.Points);
        }
    }
}
