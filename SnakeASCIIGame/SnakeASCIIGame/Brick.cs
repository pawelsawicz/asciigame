﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SnakeASCIIGame
{
    public class Brick : Shape
    {
        public bool Alive { get; set; }
        public Color Color { get; set; }
        public Rectangle Location { get; set; }
        public int Level { get; set; }

        public Brick(SpriteFont font, Rectangle screenBounds, Rectangle location, Color color, string content)
            : base(font, screenBounds)
        {
            Location = location;
            Color = color;
            Content = content;
            var temp = Position;
            temp.X = location.X;
            temp.Y = location.Y;
            Position = temp;
            Alive = true;           

        }

        public bool CheckCollision(Ball ball)
        {
            if (Alive && ball.Bound.Intersects(Location))
            {
                Alive = false;
                ball.Deflection(this);
                return true;
            }
            return false;
        }

        public override void Update()
        {
            base.Update();
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Alive)
            {
                spriteBatch.DrawString(Font, Content, Position, Color);
            }
        }
        
    }

}
