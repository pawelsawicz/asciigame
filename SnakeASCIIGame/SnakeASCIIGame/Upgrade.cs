﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SnakeASCIIGame
{
    public class Upgrade : Shape
    {
        public bool Alive { get; set; }
        public Color Color { get; set; }
        public Rectangle Location { get; set; }
        public int TypeOfUpgrade { get; set; }
        public PaddleUpgrade PaddleUpgrade { get; set; }        
        public Upgrade(SpriteFont font, Rectangle screenBounds, Rectangle location, int type)
            : base(font, screenBounds)
        {
            Speed = 3f;
            Location = location;
            TypeOfUpgrade = type;
            var temp = Position;
            temp.X = location.X;
            temp.Y = location.Y;
            Position = temp;
            Alive = true;
            SetProperties();
        }

        public void SetProperties()
        {
            switch (TypeOfUpgrade)
            {
                case 1:
                    Color = Color.Cyan;
                    Content = "(+)";
                    break;
                case 2:
                    Color = Color.Gold;
                    Content = "(-)";
                    break;
                case 37:
                    Color = Color.LemonChiffon;
                    Content = "(++)";
                    break;
                case 39:
                    Color = Color.LemonChiffon;
                    Content = "(--)";
                    break;
                case 47:
                    Color = Color.LemonChiffon;
                    Content = "(O+)";
                    break;
                case 59:
                    Color = Color.LemonChiffon;
                    Content = "(O-)";
                    break;
                case 69:
                    Color = Color.LemonChiffon;
                    Content = "(!)";
                    break;
                default:
                    Content = "empty";
                    Alive = false;
                    break;
            }
        }
        public bool PaddleCollision(Rectangle paddleLocation)
        {
            Rectangle upgradeLocation = new Rectangle(
                (int)Position.X,
                (int)Position.Y,
                (int)SizeOfString.X,
                (int)SizeOfString.Y);
            if (Alive && paddleLocation.Intersects(upgradeLocation))
            {
                Alive = false;
                return true;
            }
            return false;
        }

        public override void Update()
        {
            Motion = new Vector2(0, 1);
            var temp = Position;
            temp += (Motion * Speed);
            Position = temp;
            base.Update();
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Alive)
            {
                spriteBatch.DrawString(Font, Content, Position, Color);
            }
        }

        public void GiveUpgrade()
        {

        }
    }
}
